import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget{
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int _counter = 0;

  bool checked = false;
  AnimationController _animationController;

  Animation<double> _animation;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
      reverseDuration: Duration(seconds: 1),
    );
    _animation =
        Tween<double>(begin: 1, end: 0).animate(_animationController);

    _animationController.addListener(update);

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }


  void update() => setState(() {});

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CheckboxListTile(
              title: Text(
                'amazing list tile',
              ),
              value: checked,
              onChanged: (bool val) {
                setState(() {
                  checked = val;
                });
              },
            ),
            Table(
              border: TableBorder.all(),
              children: [
                TableRow(
                  children: [
                    Container(
                      height: 20.0,
                      width: 20.0,
                      padding: EdgeInsets.all(5.0),
                      color: Colors.yellow,
                    ),
                    Container(
                      height: 25.0,
                      width: 20.0,
                      padding: EdgeInsets.all(5.0),
                      color: Colors.yellow,
                    ),
                  ],
                ),
                TableRow(
                  children: [
                    Container(
                      height: 20.0,
                      width: 25.0,
                      padding: EdgeInsets.all(5.0),
                      color: Colors.yellow,
                    ),
                    Container(
                      height: 20.0,
                      width: 20.0,
                      padding: EdgeInsets.all(5.0),
                      color: Colors.yellow,
                    ),
                    Container(
                      height: 25.0,
                      width: 20.0,
                      padding: EdgeInsets.all(5.0),
                      color: Colors.yellow,
                    ),
                  ],
                ),
              ],
            ),
            DataTable(columns: [
              DataColumn(
                label: Text('awesome'),
              ),
              DataColumn(
                label: Text('legal'),
              )
            ], rows: [
              DataRow(
                cells: [
                  DataCell(Text('yes')),
                  DataCell(Text('yes')),
                ],
              ),
              DataRow(
                cells: [
                  DataCell(Text('no')),
                  DataCell(Text('no')),
                ],
              ),
            ]),
             FadeTransition(
               opacity: _animation,
               child: Text(
                '$_counter',
                style: TextStyle(fontSize: 7),
            ),
             ),
            SizedBox(
              height: 75,
              child: FadeInImage.assetNetwork(
                placeholder: 'assets/p724.jpg',
                image: 'https://toptattoo.ru/photos/2016/02/tatuirovka-devy-s-serdechkami-360x335.jpg',
              ),
            )
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
